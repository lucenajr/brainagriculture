FROM node:16.14.0

COPY . /var/app
WORKDIR /var/app

RUN yarn install

ENV PORT 4000
EXPOSE $PORT

CMD [ "yarn", "dev" ]




