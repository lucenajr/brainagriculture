module.exports = {
  singleQuote: true,
  trailingComma: 'all',
  allowParens: 'avoid',
  printWidth: 130,
  tabWidth: 2,
  bracketSpacing: true,
  bracketLine: true,
};
