import { validate } from 'gerador-validador-cpf';

class ValidateDocument {
  validate(document: string): boolean {
    return validate(document);
  }
}

export { ValidateDocument };
