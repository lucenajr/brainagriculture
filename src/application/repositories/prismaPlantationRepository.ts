import { prisma } from '../../database/connection';
import { IPlantationRepository, PlantationCreate, Plantation } from './implements/interfacePlantationsRepository';

class PrismaPlantationRepository implements IPlantationRepository {
  private static INSTANCE: PrismaPlantationRepository;

  private constructor() {}

  public static getInstance(): PrismaPlantationRepository {
    if (!PrismaPlantationRepository.INSTANCE) {
      return (PrismaPlantationRepository.INSTANCE = new PrismaPlantationRepository());
    }
    return PrismaPlantationRepository.INSTANCE;
  }

  async save(data: PlantationCreate): Promise<Plantation> {
    const plantation = await prisma.plantation.create({
      data,
    });

    return plantation;
  }

  async findByName(name: string): Promise<Plantation | null> {
    const plantation = await prisma.plantation.findFirst({
      where: {
        name,
      },
    });

    return plantation;
  }
}

export { PrismaPlantationRepository };
