import { prisma } from '../../database/connection';
import { IReportsRepository, Reports } from './implements/interfaceReportsRepository';

type ReducerPlantation = {
  farms_for_plantations: {
    quantity: number;
    plantation: string;
  }[];
  total: number;
};

type ReducerState = {
  plantation_for_state: {
    quantity: number;
    state: string;
  }[];
  total: number;
};

class PrismaReportsRepository implements IReportsRepository {
  private static INSTANCE: PrismaReportsRepository;

  private constructor() {}

  public static getInstance(): PrismaReportsRepository {
    if (!PrismaReportsRepository.INSTANCE) {
      return (PrismaReportsRepository.INSTANCE = new PrismaReportsRepository());
    }
    return PrismaReportsRepository.INSTANCE;
  }

  async reports(): Promise<Reports> {
    const farmsTotalHectares = await prisma.farm.aggregate({ _sum: { total_area: true } });

    const farmsQuantity = await prisma.farm.count();

    const FindfarmsAggregateForState = await prisma.farm.groupBy({
      by: ['state'],
      _count: { _all: true },
      orderBy: {
        _count: {
          state: 'desc',
        },
      },
    });

    const farmsAggregateForState = FindfarmsAggregateForState.reduce(
      (acc, value) => {
        acc.plantation_for_state.push({
          state: value.state,
          quantity: value._count._all,
        });
        acc.total += value._count._all;

        return acc;
      },
      { plantation_for_state: [], total: 0 } as ReducerState,
    );

    const findFarmsAggregateForPlantation = await prisma.cultivate.findMany({
      select: {
        plantation: {
          select: {
            name: true,
            _count: {
              select: {
                farms: true,
              },
            },
          },
        },
      },
      orderBy: {
        plantation: {
          name: 'asc',
        },
      },
      distinct: 'plantation_id',
    });

    const { farms_for_plantations, total } = findFarmsAggregateForPlantation.reduce(
      (acc, value) => {
        acc.farms_for_plantations.push({
          plantation: value.plantation.name,
          quantity: value.plantation._count.farms,
        });
        acc.total += value.plantation._count.farms;
        return acc;
      },
      { farms_for_plantations: [], total: 0 } as ReducerPlantation,
    );

    const areaArableAndVegetationArea = await prisma.farm.aggregate({
      _sum: {
        arable_area: true,
        vegetation_area: true,
      },
    });

    return {
      total_area: farmsTotalHectares._sum.total_area,
      farms_total: farmsQuantity,
      farms_for_state: farmsAggregateForState,
      farms_for_plantations: {
        total,
        values: farms_for_plantations,
      },
      farms_area_arable_and_vegetation: {
        total_area: farmsTotalHectares._sum.total_area,
        arable_area: areaArableAndVegetationArea._sum.arable_area,
        vegetation_area: areaArableAndVegetationArea._sum.vegetation_area,
      },
    };
  }
}
export { PrismaReportsRepository };
