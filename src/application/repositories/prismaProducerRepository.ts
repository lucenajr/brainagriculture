import { prisma } from '../../database/connection';
import { AppError } from '../../error';
import { IProducersRepository, ProducerCreate, ProducerSave } from './implements/interfaceProducersRepository';

class PrismaProducerRepository implements IProducersRepository {
  private static INSTANCE: PrismaProducerRepository;

  private constructor() {}

  public static getInstance(): PrismaProducerRepository {
    if (!PrismaProducerRepository.INSTANCE) {
      return (PrismaProducerRepository.INSTANCE = new PrismaProducerRepository());
    }
    return PrismaProducerRepository.INSTANCE;
  }

  async save({ document, name }: ProducerCreate): Promise<ProducerSave> {
    const producer = (await prisma.producer.create({
      data: {
        document,
        name,
      },
    })) as ProducerSave;

    return producer;
  }

  async findByDocument(document: string): Promise<ProducerSave | undefined> {
    const producer = (await prisma.producer.findFirst({
      where: {
        document,
      },
    })) as ProducerSave;

    return producer;
  }

  delete(id: number): Promise<ProducerSave> {
    return prisma.producer.delete({
      where: {
        id,
      },
    });
  }

  async update({ document, id, name }: ProducerSave): Promise<ProducerSave> {
    const producerDocumentUsed = await prisma.producer.findFirst({
      where: {
        document,
        AND: {
          id: {
            not: id,
          },
        },
      },
    });

    if (producerDocumentUsed) throw new AppError('Document is already in use');

    return prisma.producer.update({
      where: {
        id,
      },
      data: {
        name,
        document,
      },
    });
  }

  find(): Promise<ProducerSave[]> {
    return prisma.producer.findMany({
      select: {
        id: true,
        name: true,
        document: true,
        farms: {
          select: {
            name: true,
            city: true,
            state: true,
            arable_area: true,
            total_area: true,
            vegetation_area: true,
            plantations: {
              select: {
                id: true,
                plantation: {
                  select: {
                    name: true,
                  },
                },
              },
            },
          },
        },
      },
    });
  }

  async findById(id: number): Promise<ProducerSave | null> {
    return prisma.producer.findFirst({
      where: {
        id,
      },
      select: {
        id: true,
        name: true,
        document: true,
        farms: {
          select: {
            name: true,
            city: true,
            state: true,
            arable_area: true,
            total_area: true,
            vegetation_area: true,
            plantations: {
              select: {
                id: true,
                plantation: {
                  select: {
                    name: true,
                  },
                },
              },
            },
          },
        },
      },
    });
  }
}
export { PrismaProducerRepository };
