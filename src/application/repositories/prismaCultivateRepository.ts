import { prisma } from '../../database/connection';
import { Cultivate, CultivateCreate, ICultivatesRepository } from './implements/interfaceCultivatesRepository';

class PrismaCultivateRepository implements ICultivatesRepository {
  private static INSTANCE: PrismaCultivateRepository;

  private constructor() {}

  public static getInstance(): PrismaCultivateRepository {
    if (!PrismaCultivateRepository.INSTANCE) {
      return (PrismaCultivateRepository.INSTANCE = new PrismaCultivateRepository());
    }
    return PrismaCultivateRepository.INSTANCE;
  }

  async save({ farm_id, plantation_id }: CultivateCreate): Promise<Cultivate> {
    const cultivate = await prisma.cultivate.create({
      data: {
        farm_id,
        plantation_id,
      },
    });

    return cultivate;
  }

  async findCultivateByPlantation({ plantation_id, farm_id }: CultivateCreate): Promise<Cultivate | null> {
    const cultivate = await prisma.cultivate.findFirst({
      where: {
        plantation_id,
        farm_id,
      },
    });
    return cultivate;
  }
}

export { PrismaCultivateRepository };
