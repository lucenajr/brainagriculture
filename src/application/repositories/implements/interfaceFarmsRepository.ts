type FarmCreate = {
  name: string;
  city: string;
  state: string;
  total_area: number;
  arable_area: number;
  vegetation_area: number;
  producer_id: number;
};

type Farm = {
  id: number;
  name: string;
  city: string;
  state: string;
  total_area: number;
  arable_area: number;
  vegetation_area: number;
  created_at?: Date;
  updated_at?: Date | null;
  producer_id?: number;
};

interface IFarmsRepository {
  save({ name, arable_area, city, state, total_area, vegetation_area, producer_id }: FarmCreate): Promise<Farm>;
  update({ id, arable_area, city, name, state, total_area, vegetation_area }: Farm): Promise<Farm>;
  delete(id: number): Promise<Farm>;
}

export { IFarmsRepository, FarmCreate, Farm };
