type Reports = {
  total_area: number | null;
  farms_total: number;
  farms_for_state: {
    plantation_for_state: {
      state: string;
      quantity: number;
    }[];
    total: number;
  };
  farms_for_plantations: {
    total: number;
    values: { plantation: string; quantity: number }[];
  };
  farms_area_arable_and_vegetation: {
    total_area: number | null;
    arable_area: number | null;
    vegetation_area: number | null;
  };
};

interface IReportsRepository {
  reports(): Promise<Reports>;
}

export { IReportsRepository, Reports };
