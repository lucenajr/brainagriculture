type ProducerCreate = {
  name: string;
  document: string;
};

type ProducerSave = {
  id: number;
  name: string;
  document: string;
};

interface IProducersRepository {
  save({ document, name }: ProducerCreate): Promise<ProducerSave>;
  findByDocument(document: string): Promise<ProducerSave | undefined>;
  delete(id: number): Promise<ProducerSave>;
  update({ document, id, name }: ProducerSave): Promise<ProducerSave>;

  find(): Promise<ProducerSave[]>;

  findById(id: number): Promise<ProducerSave | null>;
}

export { IProducersRepository, ProducerCreate, ProducerSave };
