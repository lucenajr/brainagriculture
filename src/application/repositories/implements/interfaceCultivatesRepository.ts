type CultivateCreate = {
  farm_id: number;
  plantation_id: number;
};

type Cultivate = {
  id: number;
  farm_id: number;
  plantation_id: number;
};

interface ICultivatesRepository {
  save({ farm_id, plantation_id }: CultivateCreate): Promise<Cultivate>;
  findCultivateByPlantation({ farm_id, plantation_id }: CultivateCreate): Promise<Cultivate | null>;
}

export { ICultivatesRepository, CultivateCreate, Cultivate };
