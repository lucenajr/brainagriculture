type PlantationCreate = {
  name: string;
};

type Plantation = {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date | null;
};

interface IPlantationRepository {
  save({ name }: PlantationCreate): Promise<Plantation>;
  findByName(name: string): Promise<Plantation | null>;
}

export { IPlantationRepository, PlantationCreate, Plantation };
