import { prisma } from '../../database/connection';
import { AppError } from '../../error';
import { Farm, FarmCreate, IFarmsRepository } from './implements/interfaceFarmsRepository';

class PrismaFarmRepository implements IFarmsRepository {
    private static INSTANCE: PrismaFarmRepository;

    private constructor() {}

    public static getInstance(): PrismaFarmRepository {
        if (!PrismaFarmRepository.INSTANCE) {
            return (PrismaFarmRepository.INSTANCE = new PrismaFarmRepository());
        }
        return PrismaFarmRepository.INSTANCE;
    }

    save({ name, arable_area, city, state, total_area, vegetation_area, producer_id }: FarmCreate): Promise<Farm> {
        if (arable_area + vegetation_area > total_area)
            throw new AppError('The sum of arable area and vegetation must not be greater than the total area of the farm');

        return prisma.farm.create({
            data: {
                city,
                name,
                arable_area,
                total_area,
                vegetation_area,
                state,
                producer_id,
            },
        });
    }

    update({ id, arable_area, city, name, state, total_area, vegetation_area }: Farm): Promise<Farm> {
        if (arable_area + vegetation_area > total_area)
            throw new AppError('The sum of arable area and vegetation must not be greater than the total area of the farm');
        return prisma.farm.update({
            where: {
                id,
            },
            data: {
                arable_area,
                city,
                name,
                state,
                total_area,
                vegetation_area,
            },
        });
    }

    delete(id: number): Promise<Farm> {
        return prisma.farm.delete({
            where: {
                id,
            },
        });
    }
}

export { PrismaFarmRepository };
