import { Request, Response } from 'express';
import { PrismaFarmRepository } from '../repositories/prismaFarmRepository';
import { DeleteFarm } from '../useCases/deleteFarm';

class DeleteFarmController {
    async execute(request: Request, response: Response) {
        const { body } = request;

        const prismaRepository = PrismaFarmRepository.getInstance();
        const deleteFarmService = new DeleteFarm(prismaRepository);
        const farm = await deleteFarmService.execute(body);

        return response.json(farm);
    }
}
export { DeleteFarmController };
