import { Request, Response } from 'express';
import { PrismaProducerRepository } from '../repositories/prismaProducerRepository';
import { GetProducer } from '../useCases/getProducer';

class GetProducerController {
  async execute(request: Request, response: Response) {
    const id = Number(request.params.id as string);
    const prismaRepository = PrismaProducerRepository.getInstance();
    const getProducerService = new GetProducer(prismaRepository);
    const producer = await getProducerService.execute(id);

    return response.json(producer);
  }
}
export { GetProducerController };
