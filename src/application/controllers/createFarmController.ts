import { Request, Response } from 'express';
import { PrismaFarmRepository } from '../repositories/prismaFarmRepository';
import { CreateFarm } from '../useCases/createFarm';

class CreateFarmController {
  async execute(request: Request, response: Response) {
    const { body } = request;
    const prismaRepository = PrismaFarmRepository.getInstance();
    const createFarmService = new CreateFarm(prismaRepository);
    const farm = await createFarmService.execute(body);

    return response.json(farm);
  }
}

export { CreateFarmController };
