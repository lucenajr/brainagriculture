import { Request, Response } from 'express';
import { PrismaProducerRepository } from '../repositories/prismaProducerRepository';
import { DeleteProducer } from '../useCases/deleteProducer';

class DeleteProducerController {
    async execute(request: Request, response: Response) {
        const { body } = request;

        const prismaRepository = PrismaProducerRepository.getInstance();
        const deleteProducerService = new DeleteProducer(prismaRepository);
        const producer = await deleteProducerService.execute(body);

        return response.json(producer);
    }
}
export { DeleteProducerController };
