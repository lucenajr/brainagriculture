import { Request, Response } from 'express';
import { PrismaProducerRepository } from '../repositories/prismaProducerRepository';
import { CreateProducer } from '../useCases/createProducer';

class CreateProducerController {
  async execute(request: Request, response: Response) {
    const { body } = request;

    const prismaRepository = PrismaProducerRepository.getInstance();
    const createProducerService = new CreateProducer(prismaRepository);
    const producer = await createProducerService.execute(body);

    return response.json(producer);
  }
}
export { CreateProducerController };
