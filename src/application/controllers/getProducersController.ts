import { Request, Response } from 'express';
import { PrismaProducerRepository } from '../repositories/prismaProducerRepository';
import { GetProducers } from '../useCases/getProducers';

class GetProducersController {
  async execute(request: Request, response: Response) {
    const prismaRepository = PrismaProducerRepository.getInstance();
    const getProducerService = new GetProducers(prismaRepository);
    const producer = await getProducerService.execute();

    return response.json(producer);
  }
}
export { GetProducersController };
