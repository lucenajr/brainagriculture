import { Request, Response } from 'express';
import { PrismaProducerRepository } from '../repositories/prismaProducerRepository';
import { UpdateProducer } from '../useCases/updateProducer';

class UpdateProducerController {
  async execute(request: Request, response: Response) {
    const { body } = request;

    const prismaRepository = PrismaProducerRepository.getInstance();
    const updateProducerService = new UpdateProducer(prismaRepository);
    const producer = await updateProducerService.execute(body);

    return response.json(producer);
  }
}
export { UpdateProducerController };
