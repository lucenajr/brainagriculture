import { Request, Response } from 'express';
import { PrismaFarmRepository } from '../repositories/prismaFarmRepository';
import { UpdateFarm } from '../useCases/updateFarm';

class UpdateFarmController {
  async execute(request: Request, response: Response) {
    const { body } = request;

    const prismaRepository = PrismaFarmRepository.getInstance();
    const updateFarmService = new UpdateFarm(prismaRepository);
    const farm = await updateFarmService.execute(body);

    return response.json(farm);
  }
}
export { UpdateFarmController };
