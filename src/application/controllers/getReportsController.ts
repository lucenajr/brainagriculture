import { Request, Response } from 'express';
import { PrismaReportsRepository } from '../repositories/prismaReportRepository';
import { GetReports } from '../useCases/getReports';

class GetReportsController {
  async execute(_request: Request, response: Response) {
    const prismaRepository = PrismaReportsRepository.getInstance();
    const getReportsService = new GetReports(prismaRepository);
    const producer = await getReportsService.execute();

    return response.json(producer);
  }
}
export { GetReportsController };
