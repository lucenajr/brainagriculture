import { Request, Response } from 'express';
import { PrismaCultivateRepository } from '../repositories/prismaCultivateRepository';
import { CreateCultivate } from '../useCases/createCultivate';

class CreateCultivateController {
  async execute(request: Request, response: Response) {
    const { body } = request;
    const prismaRepository = PrismaCultivateRepository.getInstance();
    const createCultivateService = new CreateCultivate(prismaRepository);
    const cultivate = await createCultivateService.execute(body);

    return response.json(cultivate);
  }
}

export { CreateCultivateController };
