import { Request, Response } from 'express';
import { PrismaPlantationRepository } from '../repositories/prismaPlantationRepository';
import { CreatePlantation } from '../useCases/createPlantation';

class CreatePlantationController {
  async execute(request: Request, response: Response) {
    const { body } = request;

    const prismaRepository = PrismaPlantationRepository.getInstance();
    const createPlantationService = new CreatePlantation(prismaRepository);
    const plantantion = await createPlantationService.execute(body);

    return response.json(plantantion);
  }
}
export { CreatePlantationController };
