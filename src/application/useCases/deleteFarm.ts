import { IFarmsRepository } from '../repositories/implements/interfaceFarmsRepository';

type DeleteFarmRequest = {
    id: number;
};

class DeleteFarm {
    constructor(private farmsRepository: IFarmsRepository) {}

    async execute({ id }: DeleteFarmRequest) {
        return this.farmsRepository.delete(id);
    }
}

export { DeleteFarm };
