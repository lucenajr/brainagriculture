import { IFarmsRepository } from '../repositories/implements/interfaceFarmsRepository';

type UpdateFarmRequest = {
  id: number;
  name: string;
  city: string;
  state: string;
  total_area: number;
  arable_area: number;
  vegetation_area: number;
};

class UpdateFarm {
  constructor(private producersRepository: IFarmsRepository) {}

  async execute(data: UpdateFarmRequest) {
    return this.producersRepository.update(data);
  }
}

export { UpdateFarm };
