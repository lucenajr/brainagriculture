import { IReportsRepository } from '../repositories/implements/interfaceReportsRepository';

class GetReports {
  constructor(private producersRepository: IReportsRepository) {}

  async execute() {
    return this.producersRepository.reports();
  }
}

export { GetReports };
