import { AppError } from '../../error';
import { IPlantationRepository } from '../repositories/implements/interfacePlantationsRepository';

type CreatePlantationRequest = {
  name: string;
};

class CreatePlantation {
  constructor(private plantationRepository: IPlantationRepository) {}

  async execute({ name }: CreatePlantationRequest) {
    const plantationAlreadyExists = await this.plantationRepository.findByName(name);

    if (plantationAlreadyExists) throw new AppError('Plantation already exists');

    return this.plantationRepository.save({ name });
  }
}

export { CreatePlantation };
