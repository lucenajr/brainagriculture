import { InMemoryProducerRepository } from '../../test/repositories/inMemoryProducerRepository';
import { CreateProducer } from './createProducer';

describe('Create producer use case', () => {
  it('should be able to create a new producer', async () => {
    const producersRepository = new InMemoryProducerRepository();

    const sut = new CreateProducer(producersRepository);

    const response = await sut.execute({
      name: 'FAKE producer',
      document: '066.791.780-29',
    });

    expect(response).toBeTruthy();
  });
});
