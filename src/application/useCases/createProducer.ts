import { AppError } from '../../error';
import { IProducersRepository } from '../repositories/implements/interfaceProducersRepository';

type CreateProducerRequest = {
  name: string;
  document: string;
};

class CreateProducer {
  constructor(private producersRepository: IProducersRepository) {}

  async execute({ document, name }: CreateProducerRequest) {
    const producerAlreadyExists = await this.producersRepository.findByDocument(document);

    if (producerAlreadyExists) throw new AppError('Producer already exists');

    return this.producersRepository.save({ name, document });
  }
}

export { CreateProducer };
