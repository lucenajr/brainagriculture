import { IProducersRepository } from '../repositories/implements/interfaceProducersRepository';

type UpdateProducerRequest = {
  id: number;
  name: string;
  document: string;
};

class UpdateProducer {
  constructor(private producersRepository: IProducersRepository) {}

  async execute(data: UpdateProducerRequest) {
    return this.producersRepository.update(data);
  }
}

export { UpdateProducer };
