import { AppError } from '../../error';
import { ICultivatesRepository } from '../repositories/implements/interfaceCultivatesRepository';

type CreateCultivateRequest = {
  plantation_id: number;
  farm_id: number;
};

class CreateCultivate {
  constructor(private cultivatesRepository: ICultivatesRepository) {}

  async execute({ farm_id, plantation_id }: CreateCultivateRequest) {
    const cultivateAlreadyExists = await this.cultivatesRepository.findCultivateByPlantation({ farm_id, plantation_id });

    if (cultivateAlreadyExists) throw new AppError('Cultivate already exists');

    return this.cultivatesRepository.save({ farm_id, plantation_id });
  }
}

export { CreateCultivate };
