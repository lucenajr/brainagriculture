import { IProducersRepository } from '../repositories/implements/interfaceProducersRepository';

type DeleteProducerRequest = {
  id: number;
};

class DeleteProducer {
  constructor(private producersRepository: IProducersRepository) {}

  async execute({ id }: DeleteProducerRequest) {
    return this.producersRepository.delete(id);
  }
}

export { DeleteProducer };
