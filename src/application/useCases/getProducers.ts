import { IProducersRepository } from '../repositories/implements/interfaceProducersRepository';

class GetProducers {
  constructor(private producerRepository: IProducersRepository) {}

  execute() {
    return this.producerRepository.find();
  }
}

export { GetProducers };
