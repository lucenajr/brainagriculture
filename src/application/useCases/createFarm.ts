import { FarmCreate, IFarmsRepository } from '../repositories/implements/interfaceFarmsRepository';

class CreateFarm {
  constructor(private farmRepository: IFarmsRepository) {}

  execute(data: FarmCreate) {
    return this.farmRepository.save(data);
  }
}

export { CreateFarm };
