import { IProducersRepository } from '../repositories/implements/interfaceProducersRepository';

class GetProducer {
  constructor(private producerRepository: IProducersRepository) {}

  execute(id: number) {
    return this.producerRepository.findById(id);
  }
}

export { GetProducer };
