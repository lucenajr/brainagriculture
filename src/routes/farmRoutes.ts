import { Router } from 'express';
import { CreateFarmController } from '../application/controllers/createFarmController';
import { DeleteFarmController } from '../application/controllers/deleteFarmController';
import { UpdateFarmController } from '../application/controllers/updateFarmController';

const farmRouter = Router();
const farmsController = new CreateFarmController();
const updateFarmController = new UpdateFarmController();
const deleteFarmController = new DeleteFarmController();

farmRouter.post('/', farmsController.execute);

farmRouter.put('/', updateFarmController.execute);

farmRouter.delete('/', deleteFarmController.execute);

export { farmRouter };
