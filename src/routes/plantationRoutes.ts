import { Router } from 'express';
import { CreatePlantationController } from '../application/controllers/createPlantationController';

const plantationRouter = Router();
const plantationController = new CreatePlantationController();

plantationRouter.post('/', plantationController.execute);

export { plantationRouter };
