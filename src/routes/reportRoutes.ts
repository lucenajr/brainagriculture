import { Router } from 'express';
import { GetReportsController } from '../application/controllers/getReportsController';

const reportRouter = Router();
const reportController = new GetReportsController();

reportRouter.get('/', reportController.execute);

export { reportRouter };
