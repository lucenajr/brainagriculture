import { Router } from 'express';
import { CreateProducerController } from '../application/controllers/createProducerController';
import { DeleteProducerController } from '../application/controllers/deleteProducerController';
import { GetProducerController } from '../application/controllers/getProducerController';
import { GetProducersController } from '../application/controllers/getProducersController';
import { UpdateProducerController } from '../application/controllers/updateProducerController';
import { validateDocument } from '../middlewares';

const producerRouter = Router();
const createProducersController = new CreateProducerController();
const deleteProducersController = new DeleteProducerController();
const updateProducerController = new UpdateProducerController();
const getProducersController = new GetProducersController();
const getProducerController = new GetProducerController();

producerRouter.post('/', validateDocument, createProducersController.execute);
producerRouter.delete('/', deleteProducersController.execute);
producerRouter.put('/', validateDocument, updateProducerController.execute);
producerRouter.get('/all', getProducersController.execute);
producerRouter.get('/:id', getProducerController.execute);

export { producerRouter };
