import { Router } from 'express';
import { producerRouter } from './producerRoutes';
import { farmRouter } from './farmRoutes';
import { plantationRouter } from './plantationRoutes';
import { cultivateRouter } from './cultivateRoutes';
import { reportRouter } from './reportRoutes';

const router = Router();

router.use('/producer', producerRouter);
router.use('/farm', farmRouter);
router.use('/plantation', plantationRouter);
router.use('/cultivate', cultivateRouter);
router.use('/report', reportRouter);

export { router };
