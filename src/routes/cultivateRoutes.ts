import { Router } from 'express';
import { CreateCultivateController } from '../application/controllers/createCultivateController';

const cultivateRouter = Router();
const cultivatesController = new CreateCultivateController();

cultivateRouter.post('/', cultivatesController.execute);

export { cultivateRouter };
