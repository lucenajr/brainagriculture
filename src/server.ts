import 'express-async-errors';
import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { join } from 'path';
import swaggerUi from 'swagger-ui-express';

import { AppError } from './error';
import { router } from './routes';
import swaggerDocs from '../swagger.json';

const server = express();
const PORT = process.env.PORT || 4000;

server.use(express.json());
server.use(cors());
server.use('/static', express.static(join(__dirname, '..', 'public')));

server.use('/api-docs', swaggerUi.serveFiles(swaggerDocs, {}), swaggerUi.setup(swaggerDocs));
server.use(router);

server.use('*', () => {
  throw new AppError('Route not found', 404);
});

server.use((error: IErrors, _request: Request, response: Response, _next: NextFunction) => {
  if (error instanceof AppError) {
    return response.status(error.statusCode).json({
      status: 'error',
      message: error.message,
    });
  }

  if (error instanceof PrismaClientKnownRequestError) {
    let message = error.meta?.cause || `${error.meta?.target} must be unique`;

    if (error.code === 'P2003') {
      message = 'Invalid id';
    }
    return response.status(400).json({
      status: 'error',
      message,
    });
  }

  return response.status(500).json({
    status: 'error',
    message: 'Internal server error',
  });
});

server.listen(PORT, () => console.log(`Server is running on ${PORT}`));
