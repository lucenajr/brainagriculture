import { NextFunction, Request, Response } from 'express';
import { AppError } from '../error';
import { ValidateDocument } from '../utils/validateDocument';

const validateDocumentUtils = new ValidateDocument();

const validateDocument = (request: Request, response: Response, next: NextFunction) => {
  const document = (request.body.document as string) || '';
  if (!validateDocumentUtils.validate(document)) throw new AppError('Document invalid');

  next();
};

export { validateDocument };
