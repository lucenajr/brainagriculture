import { Entity } from '../../core/domain/Entity';

type PlantationProps = {
  name: string;
};

class Plantation extends Entity<PlantationProps> {
  private constructor(props: PlantationProps, id?: number) {
    super(props, id);
  }

  static create(props: PlantationProps, id?: number) {
    const plantation = new Plantation(props, id);
    return plantation;
  }
}

export { Plantation };
