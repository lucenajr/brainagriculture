import { Entity } from '../../core/domain/Entity';

type CultivateProps = {
  farm_id: string;
  plantation_id: string;
};

class Cultivate extends Entity<CultivateProps> {
  private constructor(props: CultivateProps, id?: number) {
    super(props, id);
  }

  static create(props: CultivateProps, id?: number) {
    const cultivate = new Cultivate(props, id);
    return cultivate;
  }
}

export { Cultivate };
