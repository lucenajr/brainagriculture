import { Entity } from '../../core/domain/Entity';

type FarmProps = {
    name: string;
    city: string;
    state: string;
    total_area: number;
    arable_area: number;
    vegetation_area: number;
};

class Farm extends Entity<FarmProps> {
    private constructor(props: FarmProps, id?: number) {
        super(props, id);
    }

    static create(props: FarmProps, id?: number) {
        const farm = new Farm(props, id);
        return farm;
    }
}

export { Farm };
