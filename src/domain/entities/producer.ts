import { Entity } from '../../core/domain/Entity';

type ProducerProps = {
  name: string;
  document: string;
};

class Producer extends Entity<ProducerProps> {
  private constructor(props: ProducerProps, id?: number) {
    super(props, id);
  }

  public get name(): string {
    return this.props.name;
  }

  public get document(): string {
    return this.props.document;
  }

  static create(props: ProducerProps, id?: number) {
    const producer = new Producer(props, id);
    return producer;
  }
}

export { Producer };
