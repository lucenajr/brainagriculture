declare namespace Express {
  export interface IRequest {
    ntlm: {
      DomainName: string;
      UserName: string;
      Workstation: string;
      Authenticated: boolean;
    };
  }
}
