declare interface IErrors extends Error {
  type?: string;
  statusCode: number;

  message: string;

  meta?: {
    cause?: string;
    target?: string;
  };
}
