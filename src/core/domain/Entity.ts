abstract class Entity<T> {
  public get id(): number | undefined {
    return this._id;
  }

  constructor(public props: T, protected _id?: number) {}
}

export { Entity };
