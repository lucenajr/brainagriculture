import {
  IProducersRepository,
  ProducerCreate,
  ProducerSave,
} from '../../application/repositories/implements/interfaceProducersRepository';
import { AppError } from '../../error';

class InMemoryProducerRepository implements IProducersRepository {
  delete(id: number): Promise<ProducerSave> {
    throw new AppError(`Method not implemented.${id}`);
  }

  update({ document, id, name }: ProducerSave): Promise<ProducerSave> {
    throw new AppError(`Method not implemented.${id} - ${document} - ${name}`);
  }

  all(): Promise<ProducerSave[]> {
    throw new AppError(`Method not implemented.`);
  }

  public producers: ProducerSave[] = [];

  async save(props: ProducerCreate): Promise<ProducerSave> {
    const createProducer = { ...props, id: this.producers.length + 1 };
    this.producers.push(createProducer);

    return createProducer;
  }

  async findByDocument(document: string): Promise<ProducerSave | undefined> {
    const producerFind = this.producers.find((producer) => (producer.document = document));

    return producerFind;
  }
}

export { InMemoryProducerRepository };
