/*
  Warnings:

  - The `state` column on the `farms` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- CreateEnum
CREATE TYPE "States" AS ENUM ('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RR', 'RO', 'RJ', 'RN', 'RS', 'SC', 'SP', 'SE', 'TO');

-- AlterTable
ALTER TABLE "farms" DROP COLUMN "state",
ADD COLUMN     "state" "States" NOT NULL DEFAULT E'SP';
