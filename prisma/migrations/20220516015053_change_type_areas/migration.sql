/*
  Warnings:

  - You are about to alter the column `total_area` on the `farms` table. The data in that column could be lost. The data in that column will be cast from `Decimal` to `DoublePrecision`.
  - You are about to alter the column `arable_area` on the `farms` table. The data in that column could be lost. The data in that column will be cast from `Decimal` to `DoublePrecision`.

*/
-- AlterTable
ALTER TABLE "farms" ALTER COLUMN "total_area" SET DATA TYPE DOUBLE PRECISION,
ALTER COLUMN "arable_area" SET DATA TYPE DOUBLE PRECISION;
