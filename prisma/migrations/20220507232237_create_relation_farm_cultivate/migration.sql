/*
  Warnings:

  - You are about to drop the `_FarmToPlantation` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_FarmToPlantation" DROP CONSTRAINT "_FarmToPlantation_A_fkey";

-- DropForeignKey
ALTER TABLE "_FarmToPlantation" DROP CONSTRAINT "_FarmToPlantation_B_fkey";

-- DropTable
DROP TABLE "_FarmToPlantation";

-- CreateTable
CREATE TABLE "Cultivate" (
    "id" SERIAL NOT NULL,
    "farm_id" INTEGER NOT NULL,
    "plantation_id" INTEGER NOT NULL,

    CONSTRAINT "Cultivate_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Cultivate" ADD CONSTRAINT "Cultivate_farm_id_fkey" FOREIGN KEY ("farm_id") REFERENCES "farms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Cultivate" ADD CONSTRAINT "Cultivate_plantation_id_fkey" FOREIGN KEY ("plantation_id") REFERENCES "platations"("id") ON DELETE CASCADE ON UPDATE CASCADE;
