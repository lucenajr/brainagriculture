/*
  Warnings:

  - You are about to alter the column `vegetation_area` on the `farms` table. The data in that column could be lost. The data in that column will be cast from `Decimal` to `DoublePrecision`.

*/
-- AlterTable
ALTER TABLE "farms" ALTER COLUMN "vegetation_area" SET DATA TYPE DOUBLE PRECISION;
