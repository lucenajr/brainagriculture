/*
  Warnings:

  - You are about to drop the `Cultivate` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Cultivate" DROP CONSTRAINT "Cultivate_farm_id_fkey";

-- DropForeignKey
ALTER TABLE "Cultivate" DROP CONSTRAINT "Cultivate_plantation_id_fkey";

-- DropTable
DROP TABLE "Cultivate";

-- CreateTable
CREATE TABLE "cultivates" (
    "id" SERIAL NOT NULL,
    "farm_id" INTEGER NOT NULL,
    "plantation_id" INTEGER NOT NULL,

    CONSTRAINT "cultivates_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "cultivates" ADD CONSTRAINT "cultivates_farm_id_fkey" FOREIGN KEY ("farm_id") REFERENCES "farms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cultivates" ADD CONSTRAINT "cultivates_plantation_id_fkey" FOREIGN KEY ("plantation_id") REFERENCES "platations"("id") ON DELETE CASCADE ON UPDATE CASCADE;
