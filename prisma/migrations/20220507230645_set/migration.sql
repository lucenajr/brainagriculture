-- DropForeignKey
ALTER TABLE "farms" DROP CONSTRAINT "farms_producer_id_fkey";

-- AddForeignKey
ALTER TABLE "farms" ADD CONSTRAINT "farms_producer_id_fkey" FOREIGN KEY ("producer_id") REFERENCES "producers"("id") ON DELETE CASCADE ON UPDATE CASCADE;
