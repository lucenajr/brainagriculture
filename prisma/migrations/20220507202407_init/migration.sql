-- CreateTable
CREATE TABLE "producers" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(100) NOT NULL,
    "document" VARCHAR(18) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "producers_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "farms" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(100) NOT NULL,
    "city" VARCHAR(100) NOT NULL,
    "state" VARCHAR(2) NOT NULL,
    "total_area" DECIMAL NOT NULL,
    "arable_area" DECIMAL NOT NULL,
    "vegetation_area" DECIMAL NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ,
    "producer_id" INTEGER NOT NULL,

    CONSTRAINT "farms_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "platations" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(100) NOT NULL,

    CONSTRAINT "platations_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_FarmToPlantation" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "producers_document_key" ON "producers"("document");

-- CreateIndex
CREATE UNIQUE INDEX "_FarmToPlantation_AB_unique" ON "_FarmToPlantation"("A", "B");

-- CreateIndex
CREATE INDEX "_FarmToPlantation_B_index" ON "_FarmToPlantation"("B");

-- AddForeignKey
ALTER TABLE "farms" ADD CONSTRAINT "farms_producer_id_fkey" FOREIGN KEY ("producer_id") REFERENCES "producers"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_FarmToPlantation" ADD CONSTRAINT "_FarmToPlantation_A_fkey" FOREIGN KEY ("A") REFERENCES "farms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_FarmToPlantation" ADD CONSTRAINT "_FarmToPlantation_B_fkey" FOREIGN KEY ("B") REFERENCES "platations"("id") ON DELETE CASCADE ON UPDATE CASCADE;
