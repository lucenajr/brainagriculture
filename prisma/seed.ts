import { PrismaClient } from '@prisma/client';
import faker from '@faker-js/faker/locale/pt_BR';
import { generate } from 'gerador-validador-cpf';

(async function main() {
  const prisma = new PrismaClient();
  await prisma.$connect();

  await prisma.producer.deleteMany();
  await prisma.plantation.deleteMany();

  const producersFakes = [];

  const plantationsTypes = ['Arroz', 'Feijão', 'Milho', 'Soja', 'Trigo', 'Café', 'Cana-de-açucar'];

  const plantationsTypesCreated = await prisma.plantation.createMany({
    data: plantationsTypes.map((name) => ({ name })),
  });

  const plantationsList = await prisma.plantation.findMany();

  console.log(plantationsTypesCreated);
  let total_area = 90000;
  let arable_area = 0;
  let vegetation_area = 0;

  for (let i = 0; i < 50; i++) {
    arable_area = total_area * 0.8;
    vegetation_area = total_area * 0.2;
    producersFakes.push({
      name: faker.name.findName(),
      farm: `Fazenda ${faker.company.companyName()}`,
      city: faker.address.cityName(),
      state: faker.address.stateAbbr(),
      document: generate({ format: true }),
      arable_area,
      total_area,
      vegetation_area,
      plantation: plantationsTypes[Math.floor(Math.random() * plantationsTypes.length)],
    });
    total_area -= 500;
  }

  const producersCreated = await Promise.all(
    producersFakes.map(async (producerFake) => {
      await prisma.producer.create({
        data: {
          document: producerFake.document,
          name: producerFake.name,
          farms: {
            create: {
              name: producerFake.farm,
              city: producerFake.city,
              state: producerFake.state,
              arable_area: producerFake.arable_area,
              total_area: producerFake.total_area,
              vegetation_area: producerFake.vegetation_area,
              plantations: {
                create: {
                  plantation: {
                    connectOrCreate: {
                      create: {
                        name: producerFake.plantation,
                      },
                      where: {
                        id: plantationsList.find((value) => value.name === producerFake.plantation)?.id,
                      },
                    },
                  },
                },
              },
            },
          },
        },
      });
    }),
  );

  console.log(`${producersCreated.length} Created`);

  await prisma.$disconnect();
})();
